
/*Write a Java program to create a class called Rectangle with private instance variables length and width.
Provide public getter and setter methods to access and modify these variables.*/
public class Rectangle {

    private int length;
    private int width;

    public void setLength(int newLength) {
        length = newLength;
    }

    public void setWidth(int newWidth) {
        width = newWidth;
    }
    public int getLength() {
        return length;
    }

    public int getWidth() {
        return width;
    }

    // Creating a constructor
    Rectangle(int newLength, int newWidth){
        length = newLength;
        width  = newWidth;

    }

    // Creating a method that calculates Area of a rectangle
    public int Area(){
        return length * width;
    }
}
