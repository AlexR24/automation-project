public class Countvoels {

    public static void main(String[] args) {

        String myString = "This is a random string for counting vowels";
        countingVoels(myString);
        System.out.println("Number of vowels in this string is: " + countingVoels(myString));

    }

    static int countingVoels(String vowels) {
        int count = 0;
        for (int i = 0; i < vowels.length(); i++) {

            char ch = vowels.charAt(i);
            if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u') {
                count++;
            }

        }

        return count;
    }
}
