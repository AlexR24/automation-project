public class SeeColor {

    public static void main(String[] args){

    }

    public String seeColor(String str) {

        //Using startsWith method
  /*String redString="red";
  String blueString="blue";
  if(str.startsWith(redString)){
    return redString;
  }
  else if (str.startsWith(blueString)) {
    return blueString;
  }
  else {
    return "";
  }*/

        //Not using startsWith method
        if (str.length() >=3 && str.substring(0,3).equals ("red")) {
            return "red";
        }
        else if (str.length() >=4 && str.substring(0,4).equals ("blue")) {
            return "blue";
        }
        else {
            return "";
        }
    }

}
