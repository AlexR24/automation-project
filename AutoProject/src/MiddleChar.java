public class MiddleChar {

    public static void main(String[] args){

        String myString = "This is a random string";
        midChar(myString);

        System.out.println("The middle chars are ".concat(midChar(myString)));

    }

    static String midChar(String myString) {
        String newString="";
        if (myString.length() % 2 == 0) {
            newString = myString.substring(myString.length()/2-1,myString.length()/2+1);
        } else {
            newString = myString.substring(myString.length()/2,myString.length()/2+1);
        }
       // System.out.println("The middle chars are ".concat(newString));
        return newString;
    }
}
