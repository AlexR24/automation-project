import java.util.Scanner;

public class PositiveNegative{
    public static void main(String[] args) {
        myMethod();
    }
    static void myMethod() {
        // Create a Scanner object to read input from the user
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter a number: ");

        // Read the input number from the user
        int number = scanner.nextInt();

        // Close the scanner since we no longer need it
        scanner.close();

        // Check if the number is positive, negative, or zero
        if (number > 0) {
            System.out.println("The number is positive.");
        } else if (number < 0) {
            System.out.println("The number is negative.");
        } else {
            System.out.println("The number is zero.");
        }
    }

}



