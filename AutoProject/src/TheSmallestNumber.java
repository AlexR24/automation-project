public class TheSmallestNumber {

    public static void main(String[] args) {

        int a = 6;
        int b = 10;
        int c = 56;

        int result1 = theSmallest(a, b, c);
        System.out.println("Printing the new int which is the result of applying the  theSmallest method: " + result1);
        System.out.println("Printing the result of applying theSmallest method with declared variables: " + theSmallest(a, b, c));
        System.out.println("Printing the result of applying theSmallest method with given integers: " + theSmallest(10, 20, 40));
    }

    static int theSmallest(int a, int b, int c) {
        int smallest = 0;
        if (a < b && a < c) {
            smallest = a;
        } else if (b < a && b < c) {
            smallest = b;
        } else if (c < a && c < b) {
            smallest = c;
        }

        return smallest;
    }

}

// diff between concat and + for strings
