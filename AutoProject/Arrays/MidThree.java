public class MidThree {

    public static void main(String[] args) {

    }

    public int[] midThree(int[] nums) {
        int[] myArray = new int [3];
        if (nums.length%2!=0 && nums.length>=3){
            myArray[0]=nums[nums.length/2-1];
            myArray[1]=nums[nums.length/2];
            myArray[2]=nums[nums.length/2+1];
            return myArray;
        }else {
            return nums;
        }

    }
    
}
