import java.util.Arrays;
import java.util.Random;

public class Validationexercise {
    public static void main(String[] args) {

        int newArray[] = {2,7,9,1,4,6,3,5,8,2,7,9};
        int  newDigit=0;
        int controlDigit=0;
        int[] cnpArray = getRandomArray(13);
        /*newDigit= cnpArray[0] * newArray[0] + cnpArray[1] * newArray[1] + cnpArray[2] * newArray[2] + cnpArray[3] * newArray[3]+
                cnpArray[4] * newArray[4] + cnpArray[5] * newArray[5] + cnpArray[6] * newArray[6] + cnpArray[7] * newArray[7]+
                cnpArray[8] * newArray[8] + cnpArray[9] * newArray[9] + cnpArray[10] * newArray[10] + cnpArray[11] * newArray[11];*/
        for (int i = 0; i < 12; i++) {
            newDigit += cnpArray[i] * newArray[i];
        }

        if (newDigit % 11==10){
            controlDigit=1;
            cnpArray[12]=controlDigit;
            System.out.println("This CNP is valid: " + Arrays.toString(cnpArray));
        } else if (newDigit % 11<10) {
            controlDigit=newDigit%11;
            cnpArray[12]=controlDigit;
            System.out.println("This CNP is valid: " + Arrays.toString(cnpArray));
        } else{
            System.out.println("This CNP is invalid: " + Arrays.toString(cnpArray));
        }
    }

    public static int[] getRandomArray(int len){
        Random random = new Random();
        int[] randomArray = new int[len];
        for (int i=0; i<len;i++){
            randomArray[i] = random.nextInt(10);
        }
        return randomArray;
    }
}