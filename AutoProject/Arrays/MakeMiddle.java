public class MakeMiddle {

    public static void main(String[] args) {

    }

    public int[] makeMiddle(int[] nums) {
        int[] myArray = new int[2];

        if (nums.length%2==0 && nums.length>=2){
            myArray[0] = nums[nums.length/2-1];
            myArray[1] = nums[nums.length/2];
            return myArray;
        } else{
            return nums;
        }
    }

}
