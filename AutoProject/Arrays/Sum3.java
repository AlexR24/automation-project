public class Sum3 {
    public static void main(String[] args) {
    }
    public int sum3(int[] nums) {
        int result=0;
  /* using While loop:

    while (nums.length==3){
    result= nums[0]+nums[1]+nums[2];
    break;
     }
    return result;*/

        // using For loop
        for (int i=0;i<3;i++){
            result= nums[0]+nums[1]+nums[2];
        }
        return result;
    }
}

