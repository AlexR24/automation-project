public class Plus2 {

    public static void main(String[] args){

    }

    public int[] plusTwo(int[] a, int[] b) {
        /*int[] myArray = {a[0],a[1],b[0],b[1]};
         return myArray;
         This approach is for this specific array*/

        /* the below approach is for arrays of any length, used 2 for loops,
        one for each 2 elements of the new array .*/
        int[] myArray = new int[a.length + b.length];

        for(int i=0; i < a.length; i++) {
            myArray[i] = a[i];
        }
        for(int j=2; j < b.length + a.length; j++) {
            myArray[j] = b[j-2];
        }
        return myArray;
    }


}
