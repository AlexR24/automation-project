public class Main {

    public static void main(String[] args){

        Person p = new Person();
        p.setName("Alex");
        p.setAge(29);
        p.setCountry("Ireland");

        System.out.println("This person is " + p.getName() + ", he has " + p.getAge()
        + " and he is from " + p.getCountry());

    }

}
