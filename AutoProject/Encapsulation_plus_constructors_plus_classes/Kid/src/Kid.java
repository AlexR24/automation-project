public class Kid {

    private int kidId;

    public int getKidId() {
        return kidId;
    }

    public void setKidId(int kidId) {
        this.kidId = kidId;
    }

    private String kidName;

    public String getKidName() {
        return kidName;
    }

    public void setKidName(String kidName) {
        this.kidName = kidName;
    }

    private int kidGrade;

    public int getKidGrade() {
        return kidGrade;
    }

    public int addGrade(int newgrade){
        if (kidGrade ==0) {
            return kidGrade + newgrade;
        }else {
            return kidGrade;
        }
    }

    Kid(int newKidId, String newKidName){
        kidName = newKidName;
        kidId = newKidId;
    }
}
