public class Main {

    public static void main(String[] args){


        BankAccount myBankAccount = new BankAccount("5690", 50000,"John Doe","emailaddress@domain.com", "+4007565432\n" );
        /*myBankAccount.setAccountNumber("5690");
        myBankAccount.setBalance(50000);
        myBankAccount.setOwnerName("John Doe");
        myBankAccount.setEmailAddress("emailaddress@domain.com");
        myBankAccount.setPhoneNumber("+4007565432\n");*/

        System.out.println(myBankAccount.getAccountNumber());
        System.out.println(myBankAccount.getOwnerName());
        System.out.println(myBankAccount.getBalance());
        System.out.println(myBankAccount.getEmailAddress());
        System.out.println(myBankAccount.getPhoneNumber());

        myBankAccount.deposit(500.0);
        myBankAccount.withdraw(45.89);

        BankAccount myBankAccount2 = new BankAccount("13356", 20357,"Ion Doe","iondoe@domain.com", "+402355432\n" );
        System.out.println(myBankAccount2.getAccountNumber());
        System.out.println(myBankAccount2.getOwnerName());
        System.out.println(myBankAccount2.getBalance());
        System.out.println(myBankAccount2.getEmailAddress());
        System.out.println(myBankAccount2.getPhoneNumber());

        myBankAccount.deposit(3000);
        myBankAccount.withdraw(678000);
    }
}
