public class Car {



    private String companyName;
    public String getCompanyName() {
        return companyName;
    }
    private String modelName;

    public String getModelName() {
        return modelName;
    }

    private int productYear;

    public int getProductYear() {
        return productYear;
    }

    private double carMileage;

    public double getCarMileage() {
        return carMileage;
    }

    Car(String companyName, String modelName, int productYear, double carMileage){
        this.companyName = companyName;
        this.modelName = modelName;
        this.productYear = productYear;
        this.carMileage = carMileage;
    }

}
