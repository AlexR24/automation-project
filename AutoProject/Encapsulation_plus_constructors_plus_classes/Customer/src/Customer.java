public class Customer {
    public String getName() {
        return name;
    }

    public double getCreditLimit() {
        return creditLimit;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    private String name;
    private double creditLimit;
   private  String emailAddress;

    public Customer(String newName, Double newCreditLimit, String newEmailAddress){
        name = newName;
        creditLimit = newCreditLimit;
        emailAddress = newEmailAddress;
    }

    public void OfferCredit(){
        if (creditLimit < 45699){
            System.out.println("You are eligible for a loan of: " + creditLimit);
        } else{
            System.out.println("For a loan you still need to add money until you reach: $" + creditLimit );
        }
    }

}
