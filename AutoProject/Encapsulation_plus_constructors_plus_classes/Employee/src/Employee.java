/*
* Write a Java program to create a class called Employee with private instance variables employee_id,
* employee_name, and employee_salary.
* Provide public getter and setter methods to access and modify the id and name variables,
* but provide a getter method for the salary variable that returns a formatted string.
* */
public class Employee {

    private int employeeId;
    private String employeeName;
    private double employeeSalary;

    public void setEmployeeId(int newEmployeeId){
        employeeId = newEmployeeId;
    }
    public int getEmployeeId(){
        return employeeId;
    }

    public void setEmployeeName(String newEmployeeName){
        employeeName = newEmployeeName;
    }
    public String getEmployeeName(){
        return employeeName;
    }

    public double getEmployeeSalary(){
        return employeeSalary;
    }

    public void setEmployeeSalary(double NewEmployeeSalary) {
        employeeSalary = NewEmployeeSalary;
    }

    Employee(int NewEmployeeId, String NewEmployeeName, double NewEmployeeSalary){
        employeeId = NewEmployeeId;
        employeeName = NewEmployeeName;
        employeeSalary = NewEmployeeSalary;
    }
    public String getFormattedSalary() {
        return String.format("$%.2f", employeeSalary);
    }
}
