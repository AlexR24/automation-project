public class Main {

    public static void main(String[] args){


        Circle c = new Circle(35);
        System.out.println("The radius of the circle is: " + c.getRadius() );
        System.out.println("The area of this circle is: " + c.calculateArea());
        System.out.println("The perimeter of this circle is: " + c.calculatePerimeter());
    }
}
