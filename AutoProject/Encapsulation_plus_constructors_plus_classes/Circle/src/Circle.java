/*
Write a Java program to create a class called Circle with a private instance variable radius.
Provide public getter and setter methods to access and modify the radius variable.
However, provide two methods called calculateArea() and calculatePerimeter() that return the calculated area
and perimeter based on the current radius value.
*/


public class Circle {

    private double radius;

    public double getRadius() {
        return radius;
    }

    public void setRadius(double NewRadius) {
        radius = NewRadius;
    }

    public Circle(double NewRadius){
        radius = NewRadius;
    }

    public double calculateArea(){
       return radius * radius * Math.PI;
    }

    public double calculatePerimeter(){
        return Math.PI * radius *2;
    }
}
