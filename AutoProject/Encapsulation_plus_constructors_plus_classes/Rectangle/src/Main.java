public class Main {

    public static void main(String[] args){

        Rectangle r = new Rectangle(70,20);
        System.out.println("Length of the rectangle is: "+ r.getLength());
        System.out.println("Width of the rectangle is: " + r.getWidth());
        System.out.println("Area of the rectangle is: " + r.Area());

    }
}
