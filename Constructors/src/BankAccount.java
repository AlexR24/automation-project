public class BankAccount {

    private String accountNumber;
    private double balance;
    private String ownerName;
    private String emailAddress;
    private String phoneNumber;

    BankAccount(String newaccountNumber,double newbalance,String newownerName,String newemailAddres,String newphoneNumber){
        accountNumber= newaccountNumber;
        balance = newbalance;
        ownerName = newownerName;
        emailAddress = newemailAddres;
        phoneNumber = newphoneNumber;

    }
    public String getEmailAddress() {
        return emailAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public double getBalance() {
        return balance;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public void deposit(double amount) {
        balance += amount;
        System.out.println("Deposited " + amount + " into the account. New balance: " + balance);
    }

    public void withdraw(double amount) {
        if (amount <= balance) {
            balance -= amount;
            System.out.println("Withdrawn " + amount + " from the account. " +
                    "New balance: " + balance);
        } else {
            System.out.println("Insufficient funds. Withdrawal failed.");
        }
    }
}
