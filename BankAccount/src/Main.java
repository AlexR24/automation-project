public class Main {

    public static void main(String[] args){


        BankAccount myBankAccount = new BankAccount();
        myBankAccount.setAccountNumber("5690");
        myBankAccount.setBalance(50000);
        myBankAccount.setOwnerName("John Doe");
        myBankAccount.setEmailAddress("emailaddress@domain.com");
        myBankAccount.setPhoneNumber("+4007565432\n");



        System.out.println(myBankAccount.getAccountNumber());
        System.out.println(myBankAccount.getOwnerName());
        System.out.println(myBankAccount.getBalance());
        System.out.println(myBankAccount.getEmailAddress());
        System.out.println(myBankAccount.getPhoneNumber());

        myBankAccount.deposit(500.0);
        myBankAccount.withdraw(45.89);
    }
}
