public class Main {

    public static void main(String[] args){

        Employee x = new Employee(45652,"Georgescu Mihai", 4566357.97);
        System.out.println("Employee ID is: " + x.getEmployeeId());
        System.out.println("Employee name is: " + x.getEmployeeName());
        System.out.println("Employee salary is: " + x.getEmployeeSalary());
        System.out.println("Formatted salary is: " + x.getFormattedSalary());

    }
}
